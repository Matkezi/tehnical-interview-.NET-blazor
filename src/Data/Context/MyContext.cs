﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Context
{
    public class MyContext : DbContext
    {
        public const string CURRENT_USER_EMAIL = "dev@technical.tax";

        public MyContext(DbContextOptions<MyContext> options) : base(options) { }

        // User Tables
        public DbSet<User> Users { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Purchase> Purchases { get; set; }

        // Billing Tables
        public DbSet<BillingTier> BillingTiers { get; set; }

        // Configuration
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(MyContext).Assembly);
        }
    }
}
