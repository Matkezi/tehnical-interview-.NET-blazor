using Data.Context;
using Web.Features.Authorization.Abstractions;
using Web.Features.Authorization.Services;
using Web.Features.Billing.Abstractions;
using Web.Features.Billing.Services;
using Web.Features.Reporting.Abstractions;
using Web.Features.Reporting.Services;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Infrastructure Services
            services.AddValidatorsFromAssemblyContaining<Startup>();
            services.AddDbContext<MyContext>(options =>
            {
                options.UseSqlite($"Data Source={nameof(MyContext)}.db");
                options.UseLazyLoadingProxies();
                options.EnableSensitiveDataLogging();
            });

            // Web Services
            services.AddRazorPages();
            services.AddServerSideBlazor();

            // Business Logic Services
            services.AddScoped<ICurrentUserAccessor, CurrentUserAccessor>();
            services.AddScoped<IReportViewService, ReportViewService>();
            services.AddScoped<IBillingService, BillingService>();
            services.AddScoped<IPaymentService, PaymentService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
