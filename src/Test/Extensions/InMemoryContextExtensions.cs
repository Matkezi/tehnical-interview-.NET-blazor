using System;
using Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;

namespace Test.Extensions
{
    public static class InMemoryContextExtensions
    {
        public static DbContextOptions<MyContext> CreateInMemoryOptions(string databaseName = null, InMemoryDatabaseRoot root = null)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MyContext>();

            optionsBuilder
                .UseLazyLoadingProxies()
                .ConfigureWarnings(x =>
                {
                    x.Ignore(InMemoryEventId.TransactionIgnoredWarning);
                })
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors();

            if (root is null)
                optionsBuilder.UseInMemoryDatabase(databaseName ?? Guid.NewGuid().ToString());
            else
                optionsBuilder.UseInMemoryDatabase(databaseName ?? Guid.NewGuid().ToString(), root);

            return optionsBuilder.Options;
        }

        public static MyContext CreateTestContext(string databaseName = null, InMemoryDatabaseRoot root = null)
        {
            return new MyContext(CreateInMemoryOptions(databaseName, root));
        }
    }
}